# Spark Application Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-spark-app/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-spark-app/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-spark-app/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-spark-app/commits/master)

The image is based on [rychly-docker/docker-spark](/rychly-edu/docker/docker-spark).
The version of the base image can be restricted on build by the `FROM_TAG` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-spark-app:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-spark-app" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

See [docker-compose.yml](docker-compose.yml).

Copy your Spark application (as a `*.jar` or `*.py` file) and, optionally, also the application resources (`*.jar`, `*.zip`, `*.egg`, `*.py`).

Configure the container by environment variables:

*	Spark master: `MASTER_URL`
*	JARs files and dirs imported by driver: `SPARK_JARS`
*	Python files and dirs imported by driver: `SPARK_PYFILES`
*	Spark application to run: `SPARK_APP`
