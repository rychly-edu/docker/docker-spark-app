# -*- coding: utf-8 -*-

if __name__ == "__main__":
    # Spark Session and Spark Context
    from pyspark.sql import SparkSession

    spark = SparkSession.builder \
        .appName("PySpark Demo Application") \
        .getOrCreate()
    sc = spark.sparkContext

    # Python version
    import pkgutil, platform, sys
    print("### Spark with Python version %s" % platform.python_version())
    print("### sys.path = %s" % sys.path)
    print("### 1st-level packages = %s" % sorted(map(lambda x: x[1], pkgutil.iter_modules())))

    # test RDD
    print("### testing RDD.map = %s" % sc.parallelize(range(1, 10)).map(lambda x: x*2).collect())
