#!/bin/sh

DIR=$(dirname "${0}")

. "${DIR}/spark-entrypoint-helpers.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

set_master_url
set_system_jars
set_system_pyfiles
set_driver_ports
set_python

. "${DIR}/hadoop-set-props.sh"
. "${DIR}/spark-set-props.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
wait_for
make_hdfs_dirs

export PYTHONIOENCODING="utf8"

if [ $# -eq 0 ]; then
	# interactive
	exec su - "${SPARK_USER}"
else
	# non-interactive
	exec su "${SPARK_USER}" -c "${@}"
fi
